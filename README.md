# Dotfiles

Bootstrap instructions for a new system:

```
ftp https://sz.tools/boot
```

Install dotfiles:

```
ftp -o - https://sz.tools/install | sh
```

Where appropriate, replace with `wget -qO-` or `curl -sL`.
