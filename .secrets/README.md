# Secrets

Some trivial (and non-critical) secrets are stored here.
They are encrypted using `pubkey.pem` (RSA2048).
