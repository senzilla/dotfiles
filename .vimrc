set nocompatible
set encoding=utf-8

syntax on
filetype on
filetype plugin indent on

let g:python_highlight_all = 1

set nowrap
set ruler
set colorcolumn=72,80

highlight ColorColumn ctermbg=blue guibg=blue
highlight Normal guibg=black guifg=white

set directory=~/tmp,/tmp
set backupdir=~/tmp,/tmp
set undodir=~/tmp,/tmp

let g:netrw_banner=0

set backspace=indent,eol,start

set mouse=a
set guifont=JetBrains\ Mono\ 13

map <C-S> :update<CR>
imap <C-S> <Esc>:update<CR>

map <C-Q> :quit<CR>
imap <C-Q> <Esc>:quit<CR>

au BufNewFile	Makefile*,*.mk	0r ~/tpl/mk-spdx
au BufNewFile	info		0r ~/tpl/slashpackage-info

au BufReadPost *.rdfa set syntax=html

augroup MyHighlights
	autocmd!
	autocmd VimEnter,WinEnter * hi twspace ctermbg=yellow | match twspace /\s\+$/
augroup END
