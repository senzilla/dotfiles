# If not running interactively, don't do anything
case $- in
*i*) ;;
*) return ;;
esac

# Ensure consistency of stty erase across terminal emulators
stty erase "^?"

# Environment
CVS_RSH=ssh
HOST=$(hostname -s)
LANG="en_US.UTF-8"

export CVS_RSH HOST LANG

test $(uname -s) == "OpenBSD" || . ~/.dotrc $(cat ~/.dotver)

PS1="$USER@$HOST$(test $(id -u) -eq 0 && printf '#' || printf '$') "

if command -v gpgconf 2>&1 > /dev/null; then
	export GPG_TTY="$(tty)"
	export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
	gpgconf --launch gpg-agent
fi
