# Notes

## Composer

Setup with Bedrock[1]

Reconfigure webroot to `htdocs` for compatibility (e.g. Gandi):

- `composer.json`
- `config/application.php`

## Security

```
composer require 'wpackagist-plugin/wp-2fa'
```

Secure the WP 2FA installation[2]:

- Grab the encryption key from the db
- Add to `.env`
- Amend `config/application.php` to use `env()`

## S3 compatible storage

```
composer require 'wpackagist-plugin/ilab-media-tools'
```

Enable the following settings:

- Delete Uploaded Files
- Delete From Storage
- Upload Original

[1]: https://roots.io/bedrock/
[2]: https://melapress.com/support/kb/wp-2fa-add-2fa-plugin-encryption-key-wp-config/
