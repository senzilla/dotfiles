# Option 1: Sparrow

Tools:

```
# pkg_add gradle apache-ant gcc g++ pango
```

Gradle is hard-coding "gcc" as the name of the compiler. Create links:

```
# cd /usr/local/bin
# ln -s egcc gcc
# ln -s eg++ g++
```

Libraries:

```
# pkg_add pango
```

Fails with Linux-specific libraries?

# Option 2: Nunchucks

## Build sqlcipher

Dependencies:

```
# pkg_add sqlite3 tcl sqlite3-tcl
```

Make tclsh available as just that:

```
# cd /usr/local/bin
# ln -s tclsh8.6 tclsh
```

Make readline available as editline:

```
# cd /usr/local/include
# ln -s /usr/include/readline editline
```

Build:

```
$ ./configure LDFLAGS="-lcrypto"
$ make -j6
```

## Build Cap'n Proto

```
$ git clone https://github.com/capnproto/capnproto.git
$ git checkout master
$ export AUTOCONF_VERSION=2.71
$ export AUTOMAKE_VERSION=1.16
$ autoreconf -i
$ ./configure
$ gmake -j6 CC=clang CXX=clang++
$ doas gmake install
```

## Build libmultiprocess

TODO

## Build bitcoin

Tool dependencies:

```
# pkg_add bash gmake libevent libtool boost autoconf automake python
```

Need minimum v23.0 for ARMv8 crypto support. Build:

```
$ git checkout v26.0
$ export AUTOCONF_VERSION=2.71
$ export AUTOMAKE_VERSION=1.16
$ ./autogen.sh
$ ./configure MAKE=gmake --without-gui --without-bdb --disable-zmq --with-miniupnpc=no --disable-bench --enable-module-ecdh
$ gmake -j6
```

And test:

```
$ gmake check
```

## Build tap-protocol

TODO:

- Update to upstream Bitcoin core
- Simplify root CMakeList.txt and include Bitcoin libs

## Build libnunchuck

Tool dependencies:

```
# pkg_add cmake
```

```
$ mkdir build && cd build
$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ cmake --build .
```

## Build nunchuck-desktop

Tool dependencies:

```
# pkg_add qttools
```

Qt5 dependencies:

```
# pkg_add qtmultimedia qtsvg qtquickcontrols2
```

