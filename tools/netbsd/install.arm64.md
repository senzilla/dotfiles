# Installation notes for arm64

TL;DR

1. Erase SPI
2. Install Tow-Boot to SPI
3. Write arm.img to SD card (without U-Boot)
4. Boot the installer

## Installer

1. Connect Ethernet cable via USB hub on the USB 2.0 interface
2. Format NVMe drive with GPT
3. Install X11 sets

