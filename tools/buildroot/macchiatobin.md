# Installation notes for Macchiatobin

TODO

Upstream arm-trusted-firmware patch to Buildroot for ble.mk build issue.

TL;DR

1. Patch Buildroot (file in directory)
2. Build EDK2 from Buildroot (defconfig below)
3. Write flash-image.bin to SPI

Useful documentation:

* https://github.com/tianocore/edk2-platforms/tree/master/Platform/SolidRun/Armada80x0McBin
* https://github.com/Semihalf/edk2-platforms/wiki/Build_firmware
* https://github.com/SolidRun/documentation/blob/bsp/8040/u-boot.md#to-SPI-Flash

Working defconfig at `43899226b25aeb85aeb699f0df0c7ecf8d48616c`:

```
BR2_aarch64=y
BR2_cortex_a72=y
BR2_TOOLCHAIN_EXTERNAL=y
BR2_TARGET_ARM_TRUSTED_FIRMWARE=y
BR2_TARGET_ARM_TRUSTED_FIRMWARE_PLATFORM="a80x0_mcbin"
BR2_TARGET_ARM_TRUSTED_FIRMWARE_EDK2_AS_BL33=y
BR2_TARGET_ARM_TRUSTED_FIRMWARE_ADDITIONAL_TARGETS="mrvl_flash"
BR2_TARGET_BINARIES_MARVELL=y
BR2_TARGET_EDK2=y
BR2_TARGET_EDK2_PLATFORM_SOLIDRUN_ARMADA80X0MCBIN=y
BR2_TARGET_MV_DDR_MARVELL=y
```

