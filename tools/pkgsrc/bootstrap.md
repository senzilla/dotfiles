# Bootstrap pkgsrc

## Assumptions

Packaged are managed by `sysadm` on any given system.

```
groupadd wheel
usermod -G wheel sysadm
```

## Darwin SDK

Darwin needs Command Line Tools. Installing Xcode is not necessary:
https://developer.apple.com/download/all/

Review SDK version mappings in `mk/platform/Darwin.mk` and add if necessary!

## Bootstrap

Setup the dotfile environment:

```
echo 2024Q2 > ~/.dotver
cat <<EOF > ~/.dotrc
DOTFTP="$(test $(uname -s) = Darwin && printf 'curl -sL' || printf 'ftp -o -')"
DOTPKG=$(test $(uname -s) = Darwin && printf /usr/local/pkg || printf /usr/pkg)-\${1}
DOTSRC=~/source/pkgsrc-\${1}
PATH=\${DOTPKG}/bin:\${DOTPKG}/sbin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/X11R6/bin:\${HOME}/bin
export DOTFTP DOTPKG DOTSRC DOTVER=\${1} PATH
EOF
```

Export the dotfile environment:

```
. .dotrc $(cat .dotver)
```

As `root`:

```
mkdir -p ${DOTPKG}/etc
chown -R sysadm:wheel ${DOTPKG}
```

Back as `sysadm`:

```
${DOTFTP} https://sz.tools/mk.conf > ${DOTPKG}/etc/mk.conf
${DOTFTP} https://cdn.netbsd.org/pub/pkgsrc/pkgsrc-${DOTVER}/pkgsrc-${DOTVER}.tar.gz | tar -xzf - -C ~/source
mv ~/source/pkgsrc ${DOTSRC}
```

Adjust the compiler as necessary, `clang` might have to be bootstrapped after pkgsrc.

```
cd ${DOTSRC}
./bootstrap/bootstrap \
	--full \
	--unprivileged \
	--compiler=clang \
	--prefix=${DOTPKG} \
	--gzip-binary-kit=~/source/bootstrap-${DOTVER}.tgz
```

```
cat ${DOTSRC}/work/mk.conf.example >> ${DOTPKG}/etc/mk.conf
```

## Bootstrap Clang

We want a consistent compiler across platforms. Install Clang and rebuild all dependencies:

```
cd lang/clang
bmake install
sed -i old s/^CLANGBASE/#CLANGBASE/ ${DOTPKG}/etc/mk.conf
${DOTFTP} https://sz.tools/clang.conf >> ${DOTPKG}/etc/mk.conf
```

```
cd pkgtools/pkg_rolling-replace
bmake install
pkg_admin set rebuild=yes '*'
pkg_rolling-replace -uv
```

## Baseline packages

```
mkdir -p ~/tools/pkgsrc
${DOTFTP} https://sz.tools/pkg.conf > ~/tools/pkgsrc/chk.conf
```

```
cd pkgtools/pkg_chk
bmake install
pkg_chk -asu
```
