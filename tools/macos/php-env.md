# PHP environment

A basic PHP development environment on macOS require the following
pkgsrc packages:

```
databases/php-pdo
graphics/php-gd
math/php-bcmath
www/php-curl
```
