# Installation notes

Working U-Boot build:

```
U-Boot 2017.03-armada-17.10.3-00516-g6854251 (Apr 25 2018 - 19:10:06 +0000), Build: ga-17.10.6
```

Working U-Boot environment:

```
baudrate=115200
bootargs=console=ttyS0,115200 root=/dev/mmcblk1p2 rw rootwait
bootcmd=mmc dev 1; ext4load mmc 1:1 $kernel_addr $image_name; ext4load mmc 1:1 $fdt_addr $fdt_name; setenv bootargs $console root=/dev/mmcblk1p2 rw rootwait; booti $kernel_addr - $fdt_addr
bootdelay=2
console=console=ttyS0,115200
dt_name=armada-8040-mcbin-singleshot.dtb
eth1addr=00:51:82:11:22:01
eth2addr=00:51:82:11:22:02
eth3addr=00:51:82:11:22:03
ethact=mvpp2-0
ethaddr=00:51:82:11:22:00
ethprime=eth0
fdt.dtb=armada-8040-mcbin-singleshot.dtb
fdt_addr=0x4f00000
fdt_high=0xffffffffffffffff
fdt_name=armada-8040-mcbin-singleshot.dtb
fdtcontroladdr=7f70cab8
fileaddr=5000000
filesize=923808
gatewayip=10.4.50.254
get_images=tftpboot $kernel_addr $image_name; tftpboot $fdt_addr $fdt_name; run get_ramfs
get_ramfs=if test "${ramfs_name}" != "-"; then setenv ramfs_addr 0x8000000; tftpboot $ramfs_addr $ramfs_name; else setenv ramfs_addr -;fi
hostname=marvell
image_name=Image
initrd_addr=0xa00000
initrd_size=0x2000000
ipaddr=0.0.0.0
kernel_addr=0x5000000
loadaddr=0x5000000
netdev=eth0
netmask=255.255.255.0
ramfs_addr=0x8000000
ramfs_name=-
root=root=/dev/nfs rw
rootpath=/srv/nfs/
serverip=0.0.0.0
set_bootargs=setenv bootargs $console $root ip=$ipaddr:$serverip:$gatewayip:$netmask:$hostname:$netdev:none nfsroot=$serverip:$rootpath $extra_params
stderr=serial@512000
stdin=serial@512000
stdout=serial@512000
```

