# Access Points

Assuming a 23.05 release build.

Remember to re-install user packages after sysupgrade.

Upgrade packages:

```
# opkg update
# opkg list-upgradable | awk '{print $1}' | xargs opkg upgrade
```

## Roaming

Install full version of WPAD:

```
# opkg remove --autoremove wpad-basic*
# opkg install wpad
# reboot
```

Then, configure 802.11r, 802.11k and 802.11v appropriately.
