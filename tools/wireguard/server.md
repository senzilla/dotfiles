# Wireguard server on Alpine

```
root# apk add wireguard-tools wireguard-tools-wg
```

Generate the necessary Wireguard config. Then configure the interface:

```
cat > /etc/network/interfaces.post <<EOF
auto wg0
iface wg0 inet static
	requires eth0
	use wireguard
	address 192.168.3.1
EOF
```

Allow IP forwarding, enable forwarding to and from wg0, and do NAT
from wg0 to eth0:

```
echo net.ipv4.ip_forward=1 > /etc/sysctl.d/10-wireguard.conf
/etc/init.d/sysctl restart
iptables -A FORWARD -i wg0 -j ACCEPT
iptables -A FORWARD -o wg0 -j ACCEPT
iptables -t nat -I POSTROUTING 1 -s 192.168.3.0/24 -o eth0 -j MASQUERADE
/etc/init.d/iptables save
```

To enable QR code generation:

```
echo http://dl-3.alpinelinux.org/alpine/v3.14/community >> /etc/apk/repositories
apk update
apk add libqrencode
```
