# Wireguard client on Debian

```
apt install wireguard resolvconf
cp example.conf /etc/wireguard/
systemctl enable wg-quick@example
systemctl start wg-quick@example
```
