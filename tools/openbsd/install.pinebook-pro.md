# Install on PineBook Pro

TL;DR

1. Format a USB stick with FAT
2. Copy rkspi_loader.img (from pkgsrc) onto its root[1]
3. Attach to the serial console and boot to the U-Boot prompt
4. Load the image and flash (see below)
5. Write install72.img to a new SD card
6. Boot and install the system with GPT partitioning
7. Reboot while still attached to the serial console
8. rcctl enable xenodm && reboot
9. boot> set tty fb0
10. fw_update bwfm-firmware-20200316.1.3p3.tgz[2]

Flashing the SPI device:

```
=> usb start
=> usb storage
=> fatload usb 0 0x010000 rkspi_loader.img
=> sf probe
=> sf erase 0 80000
=> sf write 0x010000 0
```

[1]: https://cdn.netbsd.org/pub/pkgsrc/packages/NetBSD/aarch64/9.3/All/u-boot-pinebook-pro-2022.01nb1.tgz
[2]: http://firmware.openbsd.org/firmware/7.2/bwfm-firmware-20200316.1.3p3.tgz
