# Yubikey on OpenBSD

## FIOD2

The `ykman fido` commands do not work well on OpenBSD. Managing the Yubikey
settings has do be done on another system.

```
ssh-keygen -t ecdsa-sk -O resident -O verify-required
```

## PKCS#11

Generate a certificate on the key:

```
pkcs11-tool --login --login-type so --keypairgen --id 1 --key-type RSA:2048
```

Export the pubkey:

```
pkcs11-tool --login --read-object --type pubkey --id 1 -o pubkey.der
```

Convert the pubkey from DER to PEM:

```
openssl rsa -pubin -inform DER -in pubkey.der -outform PEM -out pubkey.pem
```

Encrypt data with openssl:

```
openssl rsautl -encrypt -inkey pubkey.pem -pubin -in foo.txt -out foo.enc
```

Decrypt the data:

```
pkcs11-tool --login --decrypt --id 1 -m RSA-PKCS -i foo.enc
```
