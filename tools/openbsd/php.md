## PHP

Simple PHP Composer environment:

```
# pkg_add composer php php-curl php-gd php-zip
```

Enable (uncomment) extensions in `/etc/php/php-8.1.ini`
