# Installtion notes for Ultra 1

TL;DR

1. Follow INSTALL.sparc64
2. Firmware upgrade was not necessary
3. Burn cd72.iso to a CD-R disk (install72.iso is too large to boot)
4. Burn install72.iso to a second CD-R disk
4. Drop to PROM prompt by booting into SunOS and halting the system
5. Insert cd72.iso
6. ok boot cdrom
7. Install the system with plenty of swap (at least 256MB)
8. Before selecting sets, switch to the install72.iso
9. Only install bsd, base and man

