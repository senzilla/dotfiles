# Installation notes for Macchiatobin

TL;DR:

1. Flash ED2K onto SPI (see notes in tools/buildroot)
2. Use mvpp2 as the uplink
3. Install with GPT
4. Profit
